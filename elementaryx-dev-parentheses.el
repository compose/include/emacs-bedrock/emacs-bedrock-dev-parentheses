(use-package elementaryx-dev-minimal)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; org-babel support for lisp and scheme ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; We add support for lisp and scheme in org-babel
(use-package org
  :defer t
  :config
  (add-to-list 'org-babel-load-languages '(scheme . t))
  (add-to-list 'org-babel-load-languages '(lisp . t)))

;;;;;;;;;;;;;;;;;;;;;
;; paredit support ;;
;;;;;;;;;;;;;;;;;;;;;

;; The paredit package is a widely used and highly recommended package
;; for editing Lisp and Scheme in Emacs. It helps maintain the
;; structural integrity of the code by automatically managing
;; parentheses and other delimiters. This can be particularly useful
;; in Lisp-like languages, where parentheses are crucial to the
;; syntax.

;; However, paredit can feel restrictive or unintuitive at first
;; because it prevents you from accidentally deleting parentheses that
;; would break the structure of the code. This may be frustrating
;; until you get used to the editing model. In addition, it has a
;; steep learning curve: It requires learning specific keybindings and
;; behaviors, which can initially slow down your workflow if you're
;; not accustomed to it.

;; As a consequence:

;; - In any case, the package can be explictly loaded through `M-x
;;   paredit-mode'

(use-package paredit
  :defer t
  :commands paredit-mode
  :diminish ((paredit-mode . " þ")))

;; - In addition, it will be automatically loaded in scheme or lisp
;;   dialects if the
;;   `elementaryx-auto-enable-paredit-for-lisp-and-scheme' variable is
;;   set to `t'. Because of the steep learning curve, the variable is
;;   set to `nil' by default.

;;   Use `(setq elementaryx-auto-enable-paredit-for-lisp-and-scheme
;;   t)' before loaded elementaryx to make this automatic loading
;;   happen (for instance in the :init block of a use-package).
;;
;;   Example of elementaryx initialization with use-package to do so:
;;
;;   (use-package elementaryx-full
;;     :init (setq elementaryx-auto-enable-paredit-for-lisp-and-scheme t))

(defcustom elementaryx-auto-enable-paredit-for-lisp-and-scheme nil
  "Whether to initialize Paredit in ElementaryX."
  :type 'boolean
  :group 'elementaryx)

(use-package paredit
  :if elementaryx-auto-enable-paredit-for-lisp-and-scheme
  :hook ((scheme-mode . paredit-mode)
	 (emacs-lisp-mode . paredit-mode)
	 (lisp-interaction-mode . disable-paredit-mode)))

;;;;;;;;;;;;;;;;;;;;
;; geiser support ;;
;;;;;;;;;;;;;;;;;;;;

;; https://github.com/emacsmirror/geiser
;; https://guix.gnu.org/manual/en/html_node/The-Perfect-Setup.html
(use-package geiser-guile
  :hook (scheme-mode . geiser-mode)
  :custom
  (geiser-default-implementation 'guile)
  (geiser-guile-warning-level
   '(arity-mismatch unbound-variable format unused-variable)))

;; ;; Guix, guix-hpc, guix-hpc-non-free (and guile)
;; ;; https://github.com/emacsmirror/geiser
;; ;; https://guix.gnu.org/manual/en/html_node/The-Perfect-Setup.html
;; (use-package geiser-guile
;;   :defer t
;;   :config
;;   (add-to-list 'geiser-guile-load-path (expand-file-name "~/project/git.savannah.gnu.org/git/guix/"))
;;   (add-to-list 'geiser-guile-load-path (expand-file-name "~/project/gitlab.inria.fr/guix-hpc/guix-hpc/"))
;;   (add-to-list 'geiser-guile-load-path (expand-file-name "~/project/gitlab.inria.fr/guix-hpc/guix-hpc-non-free/"))
;;   (add-to-list 'geiser-guile-load-path (expand-file-name "~/project/git.savannah.gnu.org/git/guile/")))

(provide 'elementaryx-dev-parentheses)
